Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FreeSpace2_Open
Source: http://www.gog.com/gamecard/freespace_2
Comment:
 ========================================
 == GOG.com End-User License Agreement ==
 ========================================
 .
 READ THE FOLLOWING TERMS AND CONDITIONS CAREFULLY BEFORE INSTALLING THE
 PROGRAM.  This end user license agreement (this "Agreement") is a legal
 agreement between you (an individual or a single entity "You") and
 GOG.com or GOG Limited ("Company") for the accompanying software
 product which includes computer software and any associated media,
 printed materials, and/or "online" or electronic documentation
 (collectively, the "Program").  By installing, copying, or otherwise
 using the Program, you acknowledge that you have read this Agreement
 and agree to be bound by the terms.  If you do not accept or agree to
 the terms of this Agreement, do not install or use the Program.
 .
 1.License.  Company grants you a non-exclusive, non-transferable
 license to use the Program, but retains all property rights in the
 Program and all copies thereof.  This Program is licensed, not sold,
 for your personal, non-commercial use.  Your license confers no title
 or ownership in this Program and should not be construed as any sale of
 any rights in this Program.  You may not transfer, distribute, rent,
 sub-license, or lease the Program or documentation, except as provided
 herein; alter, modify, or adapt the Program or documentation, or
 portions thereof including, but not limited to, translation,
 decompiling or disassembling.  You agree not to modify or attempt to
 reverse engineer, decompile, or disassemble the Program, except and
 only to the extent that such activity is expressly permitted under
 applicable law notwithstanding this limitation.  All rights not
 expressly granted under this Agreement are reserved by Company.
 .
 2.No Warranty.  You are responsible for assessing your own computer and
 the results to be obtained therefrom.  You expressly agree that use of
 the Program is at your sole risk.  The Program is provided on an "as
 is", "as available" basis, unless such warranties are legally incapable
 of exclusion.  Company and its licensors disclaim all warranties and
 conditions, whether oral or written, express, or implied, including
 without limitation any implied warranties or conditions of
 merchantability, fitness for a particular purpose, non-infringement of
 third party rights, and those arising from a course of dealing or usage
 of trade, regarding the Program.  Company and its licensors assume no
 responsibility for any damages suffered by you, including, but not
 limited to, loss of data, items or other materials from errors or other
 malfunctions caused by Company, its licensors, licensee and/or
 subcontractors, or by your or any other participant's own errors and/or
 omissions.  Company and its licensors make no warranty with respect to
 any related software or hardware used or provided by Company in
 connection with the Program except as expressly set forth above.
 .
 3.Limitation of Liability.  You acknowledge and agree that Company and
 its licensors shall not assume or have any liability for any action by
 Company or its content providers, other participants, or other
 licensors with respect to conduct, communication, or content of the
 Program.  Company and its licensors shall not be liable for any
 indirect, incidental, special, punitive, exemplary, or consequential
 damages resulting hereunder in any manner, even if advised of the
 possibility of such damages.  Except as expressly provided herein,
 Company's and its licensors' entire liability to you and your exclusive
 remedy for any breach of this Agreement is limited solely to the total
 amount paid by you for the Program, if any.  Because some states do not
 allow the exclusion or limitation of liability for certain damages, in
 such states Company's and its licensors' liability is limited to the
 extent permitted by law.
 .
 4.Indemnity.  At Company's request, you agree to defend, indemnify and
 hold harmless Company, its affiliates and licensors from all damages,
 losses, liabilities, claims and expenses, including attorneys' fees,
 arising directly or indirectly from your acts and omissions to act in
 using the Program pursuant to the terms of this Agreement or any breach
 of this Agreement by you.
 .
 5.Termination.  Without prejudice to any other rights of Company, this
 Agreement and your right to use the Program may automatically terminate
 without notice from Company if you fail to comply with any provision of
 this Agreement or any terms and conditions associated with the Program.
 In such event, you must destroy all copies of this Program and all of
 its component parts.
 .
 6.Injunction.  Because Company would be irreparably damaged if the
 terms of this Agreement were not specifically enforced, you agree that
 Company shall be entitled, without bond, other security or proof of
 damages, to appropriate equitable remedies with respect to breaches of
 this Agreement, in addition to such other remedies as Company may
 otherwise have under applicable laws.
 .
 7.General Provisions.  Company's failure to enforce at any time any of
 the provisions of this Agreement shall in no way be construed to be a
 present or future waiver of such provisions, nor in any way affect the
 right of any party to enforce each and every such provision thereafter.
 The express waiver by Company of any provision, condition or
 requirement of this Agreement shall not constitute a waiver of any
 future obligation to comply with such provision, condition or
 requirement.  This Agreement shall be governed by the laws of the State
 of California and the United States without regard to its conflicts of
 laws rules and you consent to the exclusive jurisdiction of the courts
 in Los Angeles County, California.  The United Nations Convention on
 Contracts for the International Sale of Goods shall not apply to this
 Agreement.  This Agreement represents the complete agreement concerning
 this License Agreement between you and Company.

Files: debian/*
Copyright: 2011-2013 Dmitry Smirnov <onlyjob@member.fsf.org>
License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy of
 this software and associated documentation files (the "Software"), to deal in
 the Software without restriction, including without limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 the Software, and to permit persons to whom the Software is furnished to do so,
 subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
Comment:
 This license is also known as "MIT" however FSF considers "MIT" labelling
 ambiguous and copyright-format specification recommend to label such a license
 as "Expat".

Files: *
Copyright: Volition, Inc. 1999.  All rights reserved.
License: FreeSpace-2-EULA
 SOFTWARE USE LIMITATIONS AND LIMITED LICENSE
 .
 General Product License. This copy of FreeSpace 2 (the "Software") is
 intended solely for your personal non-commercial home entertainment use.
 You may not decompile, reverse engineer, or disassemble the Software,
 except as permitted by law. Interplay Entertainment Corp. and its
 licensors retain all right, title and interest in the Software including
 all intellectual property rights embodied therein and derivatives thereof.
 The Software, including, without limitation, all code, data structures,
 characters, images, sounds, text, screens, game play, derivative works and
 all other elements of the Software may not be copied, resold, rented,
 leased, distributed (electronically or otherwise), used on a pay-per-play,
 coin-op or other for-charge basis, or for any commercial purpose.  Any
 permissions granted herein are provided on a temporary basis and can be
 withdrawn by Interplay Productions at any time. All rights not expressly
 granted are reserved.
 .
 Modem and Network Play. If the Software contains modem or network play,
 you may play the Software via modem transmission with another person or
 persons directly without transmission through a third party service or
 indirectly through a third party service only if such service is an
 authorized licensee of Interplay. For the purpose of this license, a
 "third party service" refers to any third party service which provides a
 connection between two or more users of the Software, manages, organizes,
 or facilitates game play, translates protocols, or otherwise provides a
 service which commercially exploits the Software, but does not include a
 third party service which merely provides a telephonic connection (and
 nothing more) for modem or network play. Authorized licensee services
 are listed on the Interplay Entertainment Corp. World Wide Web Site
 located at http://www.interplay.com. This limited right to transmit the
 Software expressly excludes any transmission of the Software of any data
 streams thereof on a commercial basis, including, without limitation,
 transmitting the Software by way of a commercial service (excepting those
 specific commercial services licensed by Interplay) which translates the
 protocols or manages or organizes game play sessions.
 .
 Acceptance of License Terms. By downloading or acquiring and then retaining
 this Software, you assent to the terms and restrictions of this limited
 license. If you acquired the Software and do not accept the terms of this
 limited license, you must return the Software together with all packaging,
 manuals and other material contained therein to the store where you acquired
 the Software for a full refund and if you downloaded the Software, you must
 delete it.
 .
 ...................................................................
 .
 The below version of the FS2 EULA was displayed during
 the installation process, when installing from the retail CDs.
 It is presented here for archival purposes, but has been replaced
 by the license presented above. The most notable difference between
 the two versions is the absence of the so-called friends clause
 in the current revision of the EULA. Because Interplay granted
 all rights in the original EULA on a temporary basis, even those
 who installed the game from the retail CDs may not use the friends
 clause to distribute game data.
 .
 ...................................................................
 .
 This software product, FreeSpace 2 (the "Software"), is
 intended solely for your personal noncommercial home entertainment
 use. You may not decompile, reverse engineer, or disassemble the
 Software, except as permitted by law. Interplay Productions and
 Volition, Inc. retain all rights and title in the Software including
 all intellectual property rights embodied therein and derivatives
 thereof.  You are granted a revocable, nonassignable limited license
 to create derivative works of this Software solely for your own
 personal noncommercial home entertainment use and may publicly
 display such derivative works to the extent specifically
 authorized by Interplay in writing. A copy of this authorization, if
 any, will be provided on Interplay's World Wide Web site, located at
 http://www.interplay.com, or by contacting the legal department of
 Interplay Productions in the US at (949) 553-6655. The Software,
 including, without limitation, all code, data structures, characters,
 images, sounds, text, screens, game play, derivative works and all
 other elements of the Software may not be copied (except as provided
 below), resold, rented, leased, distributed (electronically or
 otherwise), used on pay-per-play, coin-op or other for-charge basis,
 or for any commercial purpose. You may make copies of the Software
 for your personal noncommercial home entertainment use and to give to
 friends and acquaintances on a no cost noncommercial basis. This
 limited right to copy the Software expressly excludes any copying or
 distribution of the Software on a commercial basis, including,
 without limitation, bundling the product with any other product or
 service and any give away of the Software in connection with another
 product or service. Any permissions granted herein are provided on a
 temporary basis and can be withdrawn by Interplay Productions at any
 time. All rights not expressly granted are reserved.
 .
 Modem and Network Play. If the Software contains modem or network
 play, you may play the Software via modem transmission with another
 person or persons directly without transmission through a third party
 service or indirectly through a third party service only if such
 service is an authorized licensee of Interplay. For the purposes of
 this license, a third party service refers to any third party service
 which provides a connection between two or more users of the
 Software, manages, organizes, or facilitates game play, translates
 protocols, or otherwise provides a service which commercially
 exploits the Software, but does not include a third party service
 which merely provides a telephonic connection (and nothing more) for
 modem or network play. Authorized licensee services are listed on
 the Interplay Productions World Wide Web Site located at
 http://www.interplay.com. This limited right to transmit the Software
 expressly excludes any transmission of the Software or any data
 streams thereof on a commercial basis, including, without limitation,
 transmitting the Software by way of a commercial service (excepting
 those specific commercial services licensed by Interplay) which
 translates the protocols or manages or organizes game play sessions.
 If you would like information about obtaining a pay-for-play or
 commercial license to the Software, please contact Interplay
 Productions in the US at +(949) 553-6655.  Nothing in this paragraph
 is intended to prevent you from downloading the Software from
 Interplay's Web site or from commercial service providers authorized
 by Interplay to provide the Software to you.
 .
 Acceptance of License Terms. By downloading or acquiring and then
 retaining this Software, you assent to the terms and restrictions of
 this limited license. If you acquired the Software and do not accept
 the terms of this limited license, you must return the Software
 together with all packaging, manuals and other material contained
 therein to the store where you acquired the Software for a full
 refund and if you downloaded the Software, you must delete it.
 .
 Trademark and copyright (c) 1999 Volition, Inc.  All rights reserved.
